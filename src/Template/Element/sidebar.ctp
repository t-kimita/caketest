<?php
$articlesMenu = [
    'List Articles' => ['controller' => 'Articles', 'action' => 'index'],
    'New Article'   => ['controller' => 'Articles', 'action' => 'add'],
];
if (isset($addArticlesMenu)) {
    $articlesMenu = array_merge_recursive($articlesMenu, $addArticlesMenu);
}

$tagsMenu = [
    'List Tag'      => ['controller' => 'Tags', 'action' => 'index'],
    'New Tag'       => ['controller' => 'Tags', 'action' => 'add'],
];
if (isset($addTagsMenu)) {
    $tagsMenu = array_merge_recursive($tagsMenu, $addTagsMenu);
}

?>

<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>

        <?php foreach ($articlesMenu as $label => $params) { ?>
            <li><?= $this->Html->link(__($label), $params) ?></li>
        <?php } ?>

        <?php foreach ($tagsMenu as $label => $params) { ?>
            <li><?= $this->Html->link(__($label), $params) ?></li>
        <?php } ?>
    </ul>
</nav>


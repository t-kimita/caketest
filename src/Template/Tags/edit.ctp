<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tag $tag
 */
?>
<div class="tags form large-10 medium-9 columns content">
    <?= $this->Form->create($tag) ?>
    <fieldset>
        <legend><?= __('Edit Tag') ?></legend>

        <ul class="sub-nav inline-list right">
            <li><?= $this->Html->link(__('[View]'), ['action' => 'view', $tag->id]) ?></li>
            <li><?= $this->Form->postLink(__('[Delete Tag]'), ['action' => 'delete', $tag->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tag->id)]) ?></li>
        </ul>
        <div class="right"><?php echo 'Id: #'.$tag->id.''; ?></div>

        <?php
            echo $this->Form->control('title');
            echo $this->Form->control('articles._ids', ['options' => $articles]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<div class="tags form large-10 medium-9 columns content">
    <?= $this->Form->create($article) ?>
    <fieldset>
        <legend><?= __('記事の編集') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['type' => 'hidden']);
            echo $this->Form->control('title');
            echo $this->Form->control('body', ['rows' => '3']);
            echo $this->Form->control('tags._ids', ['options' => $tags]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Save Article')) ?>
    <?= $this->Form->end() ?>
</div>

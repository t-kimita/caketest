<div class="tags form large-9 medium-8 columns content">
<?= $this->Form->create($article) ?>
    <fieldset>
        <legend><?= __('記事の追加') ?></legend>
        <?php
            // 今はユーザーを直接記述
            echo $this->Form->control('user_id', ['type' => 'hidden', 'value' => 1]);
            echo $this->Form->control('title');
            echo $this->Form->control('body', ['rows' => '3']);
            echo $this->Form->control('tags._ids', ['options' => $tags]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Save Article')) ?>
<?= $this->Form->end() ?>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: "Tatsuhiko Kimita <nannbo@crappo.net>"
 * Date: 2019-01-22
 * Time: 09:43
 */

namespace Package\Utils\CakePHP;

use Cake\Log\Log as CakeLog;

class Log
{
    static public function debug($arg)
    {
        CakeLog::debug(var_export($arg, true));
    }

}
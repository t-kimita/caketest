<?php
/**
 * Created by PhpStorm.
 * User: "Tatsuhiko Kimita <nannbo@crappo.net>"
 * Date: 2019-01-22
 * Time: 10:51
 */

namespace Package\Utils\CakePHP;

use Cake\Core\Configure;

class Env
{
    public function getAppNamespace()
    {
        return Configure::read('App.namespace');
    }
}